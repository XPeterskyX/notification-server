package pl.lodz.p.se;

import static spark.Spark.*;

import org.json.JSONException;
import org.json.JSONObject;
import spark.utils.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by piotr on 17.11.15.
 */
public class NotificationServer {
    public static final String API_KEY = "AIzaSyBSnJEE2LuQRZGqcFya8NI6rTaDAdRceAU";

    public static void main(String[] args) {
        post("/notify", (request, response) -> {
            try {
                JSONObject jGcmData = new JSONObject(request.body());
                System.out.println(jGcmData.toString());
                URL url = new URL("https://android.googleapis.com/gcm/send");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Authorization", "key=" + API_KEY);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);

                // Send GCM message content.
                OutputStream outputStream = conn.getOutputStream();
                outputStream.write(jGcmData.toString().getBytes());

                // Read GCM response.
                InputStream inputStream = conn.getInputStream();
                String resp = IOUtils.toString(inputStream);
                System.out.println(resp);
                return resp;
            }catch(JSONException e){
                JSONObject resp = new JSONObject();
                resp.put("success",0);
                resp.put("reason", e.getMessage());
                return  resp;
            }catch (IOException e) {
                JSONObject resp = new JSONObject();
                resp.put("success",0);
                resp.put("reason", "Unable to send GCM message."+"\n"+
                        "API key, and that the device's registration token is correct (if specified).\n"+
                        e.getMessage());
                return  resp;
            }
        });
    }
}
